// import theme from 'mdx-deck/themes'
import { dark } from 'mdx-deck/themes'
import okaidia from 'react-syntax-highlighter/styles/prism/okaidia'

export default {
  // ...theme,
  ...dark,
  prism: {
    style: okaidia
  },
  colors: {
    ...dark.colors,
    background: 'rgba(0,0,0,0.9)'
  }
  // Customize your presentation theme here.
  //
  // Read the docs for more info:
  // https://github.com/jxnblk/mdx-deck/blob/master/docs/theming.md
  // https://github.com/jxnblk/mdx-deck/blob/master/docs/themes.md
};
