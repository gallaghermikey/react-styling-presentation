# React Styling
- Basis: https://varun.ca/styled-system/
- Also nice: https://github-ds.now.sh/
- Great thread on styled-system: https://spectrum.chat/thread/b9afb6f3-a675-4f97-bc78-66411292fab1
  - "styled system is meant to help ensure you're using scales and values consistently across our apps"

## Goals
- Show system for using consistent scales and values across our apps
- Create a more common language between design and development side
- 
- NOTE: I won't have all the answers here
- Provide guidance for how we write our components
  - e.g. how do we extend? should all components be exported as styled-components?
  - See good examples on _how_ we can extend things, e.g. using 'as'

## Hmmmm...
 - Understand the tradeoff styled-system brings
 	- More styling will occur in via props API
 	- You don't have to name as many things
 - Important! Mention that components shouldn't really be concerned with styles outside of themselves
   - e.g. Button probably shouldn't care if it's next to another Button
   - instead, make a ButtonGroup component which lays out Buttons

## Outline
- Problems
  - We're all writing slightly different CSS
  - Not everyone may want write all the required CSS (vbox, hbox from Ext)
    - Light abstractions might be useful
  - Poor reuse:
    - Creating styled-components with one or two attributes set
    - 'I just need some padding around this group of buttons'
  - How do we expose CSS props?
    - ex of every CSS attribute taking props

- Solution
  - Emotion's approach
    - `css` prop
    - Do we want this though? Why doesn't SC allow this?
  - Easier, more consistent styling through PROPS
  - Primitive layout components
  
  - `Box` evolution
```
import styled, { css } from 'styled-components';

const Box = styled.div`
  color: ${props => props.color};
  backgroundColor: ${props => props.color};
`;

---

const getColor = props => css`
  color: ${props.color};
  background-color: ${props.color};
`;https://blog.prototypr.io/a-framework-for-creating-a-predictable-and-harmonious-spacing-system-8eee8aaf773c

// OR

const getColor = props => ({
  color: props.color,
  backgroundColor: props.color
});

const Box = styled.div`
  ${getColor}
`;

---

// Let's take this further
getColor.propTypes = {
  color: propTypes.string
};

Box.propTypes = {
  ...getColor.propTypes
};

```

# Further reading
- [Space in Design Systems](https://medium.com/eightshapes-llc/space-in-design-systems-188bcbae0d62) 
- [Creating a Predictable and Harmonious Spacing System](https://blog.prototypr.io/a-framework-for-creating-a-predictable-and-harmonious-spacing-system-8eee8aaf773c) 
